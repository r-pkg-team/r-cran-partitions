Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: partitions
Upstream-Contact: Robin K. S. Hankin <hankin.robin@gmail.com>
Source: https://cran.r-project.org/package=partitions
Files-Excluded: build/* inst/doc/* vignettes/*.pdf vignettes/answers.Rdata
Comment: Vignettes will be re-generated during build.

Files: *
Copyright: 2021 Robin K. S. Hankin <hankin.robin@gmail.com>
           2021 Paul Egeler <paulegeler@gmail.com>
License: GPL-2+
Comment: Although upstream only provides the version-less "GPL", which is
 generally interpreted as GPL-1+, the "License" field on the CRAN website
 (see Source above) expands this to "GPL-2 | GPL-3".

Files:vignettes/setpartitions.Rnw
Copyright: 2021 Robin K. S. Hankin <hankin.robin@gmail.com>
           Luke G. West
License: GPL-2+
Comment: The copyright holders of this file are the authors as mentioned in the same file

Files: debian/*
Copyright: 2021-2022 Doug Torrance <dtorrance@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 2 at /usr/share/common-licenses/GPL-2.
